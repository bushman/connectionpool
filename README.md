# ConnectionPool

Last commit is a trial to create a generic Connection pool.
Uses Java dynamic proxy, so should be parametrized with interface only. For possibility of parametrizing with classes cglib or ByteBuddy proxy may be used.
Also equals by object identity doesn't work correctly (connection is compared to proxy which always results in false answer). For correct equals, one must implement equals and hash code in connection object, which takes possibility of proxying into account.