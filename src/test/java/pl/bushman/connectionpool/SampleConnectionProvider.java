package pl.bushman.connectionpool;

import java.util.concurrent.atomic.AtomicInteger;

public class SampleConnectionProvider implements ConnectionProvider<SampleConnection> {

    private AtomicInteger i = new AtomicInteger();

    @Override
    public SampleConnection getConnection() {
        return new SampleConnectionImpl(i.getAndIncrement());
    }
}
