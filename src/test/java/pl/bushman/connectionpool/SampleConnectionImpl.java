package pl.bushman.connectionpool;

class SampleConnectionImpl implements SampleConnection {
    private final int id;

    SampleConnectionImpl(int id) {
        this.id = id;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public void close() {
        throw new RuntimeException("original close");
    }
}
