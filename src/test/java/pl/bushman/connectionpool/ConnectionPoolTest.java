package pl.bushman.connectionpool;

import org.junit.jupiter.api.Test;

import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConnectionPoolTest {

    private static final int CONNECTION_ID = 123;
    private static final int POOL_SIZE = 30;

    @Test
    public void returnsConnectionFromProvider() {
        SampleConnection connection = new SampleConnectionImpl(CONNECTION_ID);
        ConnectionProvider<SampleConnection> connectionProvider = mock(ConnectionProvider.class);
        when(connectionProvider.getConnection()).thenReturn(connection);

        ConnectionPool<SampleConnection> connectionPool = new ConnectionPool<>(connectionProvider, POOL_SIZE);

        SampleConnection resultConnection = getConnectionWithTimeout(connectionPool);

        assertEquals(CONNECTION_ID, resultConnection.id());
    }

    @Test
    public void doesntReturnMoreConnectionsThanPoolSize() {
        ConnectionPool<SampleConnection> connectionPool = new ConnectionPool<>(new SampleConnectionProvider(), POOL_SIZE);

        aquireAllConnections(connectionPool);

        assertThrows(RuntimeException.class, () -> getConnectionWithTimeout(connectionPool));

    }

    @Test
    public void releasesConnections() throws IOException {
        ConnectionPool<SampleConnection> connectionPool = new ConnectionPool<>(new SampleConnectionProvider(), POOL_SIZE);

        List<SampleConnection> connections = aquireAllConnections(connectionPool);

        SampleConnection connectionToBeReleased = connections.get(13);
        connectionToBeReleased.close();

        SampleConnection connection = getConnectionWithTimeout(connectionPool);

        assertEquals(connectionToBeReleased.id(), connection.id());

    }

    @Test
    public void isThreadSafe() {
        ConnectionPool<SampleConnection> connectionPool = new ConnectionPool<>(new SampleConnectionProvider(), POOL_SIZE);

        int numThreads = 100;

        List<Thread> threads = connUsers(numThreads, connectionPool);
        start(threads);

        assertTimeout(Duration.of(10, ChronoUnit.SECONDS), () -> {
            waitUntilFinished(threads);

            assertAllConnectionsAvailable(connectionPool);
        });
    }

    private <T extends Closeable> T getConnectionWithTimeout(ConnectionPool<T> connectionPool) {
        try {
            return CompletableFuture.supplyAsync(() -> connectionPool.getConnection()).get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    private void waitUntilFinished(List<Thread> threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    private void start(List<Thread> threads) {
        threads.stream().forEach(Thread::start);
    }

    private List<SampleConnection> assertAllConnectionsAvailable(ConnectionPool<SampleConnection> connectionPool) {
        return aquireAllConnections(connectionPool);
    }

    private List<SampleConnection> aquireAllConnections(ConnectionPool<SampleConnection> connectionPool) {
        List<SampleConnection> connections = IntStream.range(0, POOL_SIZE).mapToObj(i ->
                getConnectionWithTimeout(connectionPool)
        ).collect(Collectors.toList());
        return connections;
    }

    private List<Thread> connUsers(int numThreads, ConnectionPool<SampleConnection> connectionPool) {
        return IntStream.range(0, numThreads)
                .mapToObj(i -> new Thread(() -> {
                    try (SampleConnection connection = connectionPool.getConnection()) {
                        sleepRandomTime();
                    } catch (IOException | InterruptedException e) {
                        new RuntimeException(e);
                    }
                })).collect(Collectors.toList());
    }

    private void sleepRandomTime() throws InterruptedException {
        long milis = (long) (Math.random() * 100);
        Thread.sleep(milis);
    }

}
