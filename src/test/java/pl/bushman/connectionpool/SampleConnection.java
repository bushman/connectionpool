package pl.bushman.connectionpool;

import java.io.Closeable;

public interface SampleConnection extends Closeable {
    int id();
}
