package pl.bushman.connectionpool;

import java.io.Closeable;

public interface ConnectionProvider<T extends Closeable> {
     T getConnection();
}
