package pl.bushman.connectionpool;

import java.io.Closeable;
import java.lang.reflect.Proxy;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.IntStream;

public class ConnectionPool<T extends Closeable> {

    private final BlockingQueue<T> freeConnections;

    public ConnectionPool(ConnectionProvider<T> connectionProvider, int size) {
        freeConnections = new ArrayBlockingQueue<>(size);
        IntStream.range(0, size)
                .forEach(i -> freeConnections.add(proxy(connectionProvider.getConnection())));
    }

    public T getConnection() {
        try {
            return freeConnections.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    @SuppressWarnings("unchecked")
    private T proxy(T connection) {
        return (T) Proxy.newProxyInstance(
                getClass().getClassLoader()
                , connection.getClass().getInterfaces()
                , (proxy, method, args) -> {
                    if (method.getDeclaringClass().equals(Closeable.class)) {
                        freeConnections.add((T) proxy);
                        return null;
                    } else {
                        return method.invoke(connection, args);
                    }
                });

    }


}
